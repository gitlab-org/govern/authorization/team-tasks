This is the ~"group::authorization" planning template for MILESTONE. This issue is primarily for [Cross-functional](https://about.gitlab.com/handbook/engineering/development/sec/anti-abuse/#cross-functional-backlog) prioritization and discussion.

## Goals and outcomes

#### Community Contributions

## Token Permissions for Job Tokens

```glql
---
display: table
fields: title, state, assignee, weight, labels("permissions::job tokens", "priority::1")
---
group = "gitlab-org"
AND opened = true
AND label = ("type::feature", "group::authorization", "priority::1", "permissions::job tokens")
AND label in ("workflow::ready for development", "workflow::in dev")
AND milestone = "MILESTONE"
```

## Custom Admin Role

```glql
---
display: table
fields: title, state, assignee, weight, labels("permissions::custom admin role", "priority::1")
---
group = "gitlab-org"
AND opened = true
AND label = ("type::feature", "group::authorization", "priority::1","permissions::custom admin role")
AND label in ("workflow::ready for development", "workflow::in dev")
AND milestone = "MILESTONE"
```

## Custom Roles

```glql
---
display: table
fields: title, state, assignee, weight, labels("permissions::custom roles", "priority::1")
---
group = "gitlab-org"
AND opened = true
AND label = ("type::feature", "group::authorization", "priority::1", "permissions::custom roles")
AND label in ("workflow::ready for development", "workflow::in dev")
AND milestone = "MILESTONE"
```

## Stretch

```glql
---
display: table
fields: title, state, assignee, weight, labels("permissions::*", "Stretch")
---
group = "gitlab-org"
AND opened = true
AND label = ("group::authorization", "priority::2", "Stretch")
```

## Refinement

```glql
---
display: table
fields: title, state, assignee, weight, labels("permissions::*", "priority::1")
---
group = "gitlab-org"
AND opened = true
AND label = ("group::authorization", "priority::1", "workflow::refinement")
```

## Others (Bugs, Security, Performance, etc)

```glql
---
display: table
fields: title, state, assignee, weight, labels
---
group = "gitlab-org"
AND opened = true
AND label = ("group::authorization", "priority::1", "type::bug")
```

## UX

```glql
---
display: table
fields: title, state, assignee, weight, labels("workflow::*")
---
group = "gitlab-org"
AND opened = true
AND label = ("group::authorization", "priority::1")
AND label in ("workflow::ready for design", "workflow::design", "workflow::solution validation")
AND milestone = "MILESTONE"
```

## Technical Writing

```glql
---
display: table
fields: title, state, assignee, weight, labels("Technical Writing")
---
group = "gitlab-org"
AND opened = true
AND label = ("group::authorization", "Technical Writing", "priority::1" )
AND milestone = "MILESTONE"
```

## Tasks

- [ ] PM: Create issue with the title "X.X SSCS::Authorization - Planning Issue"
- [ ] PM: Set due date with end of milestone
- [ ] PM: Update GLQL with targeted milestone.
- [ ] PM: Write goals, deliverables, and community contributions.
- [ ] PD: Set ~"priority::1" label, weight, and milestone on UX work after scope is determined from UX meeting
- [ ] TW: Set ~"priority::1" & ~"Technical Writing" labels, and milestone
- [ ] EM: Confirm there is enough work for engineering by evaluating weights.
- [ ] EM: Confirm refined issues have the labels ("priority::1","permissions::") and milestone.

/assign @jayswain @jrandazzo @ipelaez1
/label ~"group::authorization" ~"Planning Issue" ~"type::ignore"
/cc @gitlab-org/govern/authorization
