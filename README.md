```
_______         _____ ______                 _____               _____ _____
___    |____  ____  /____  /_ ______ ___________(_)____________ ___  /____(_)______ _______
__  /| |_  / / /_  __/__  __ \_  __ \__  ___/__  / ___  /_  __ `/_  __/__  / _  __ \__  __ \
_  ___ |/ /_/ / / /_  _  / / // /_/ /_  /    _  /  __  /_/ /_/ / / /_  _  /  / /_/ /_  / / /
/_/  |_|\__,_/  \__/  /_/ /_/ \____/ /_/     /_/   _____/\__,_/  \__/  /_/   \____/ /_/ /_/
```

:wave: Welcome to the authorization team tasks project.

### Resources

* [Planning Issues](https://gitlab.com/gitlab-org/govern/authorization/team-tasks/-/issues/?label_name%5B%5D=Planning%20Issue)
* [Group Page](https://about.gitlab.com/handbook/engineering/development/sec/govern/authorization/)
* [Automated Reports](https://gitlab.com/groups/gitlab-org/govern/authorization/-/epics/1)
